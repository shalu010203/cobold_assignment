import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-staff',
  templateUrl: './list-staff.component.html',
  styleUrls: ['./list-staff.component.css']
})
export class ListStaffComponent implements OnInit {

  AddStaffForm: FormGroup;
  editregisterForm: FormGroup;
  submitted = false;
  AddStaffDiv: boolean = false;
  ListStaffDiv:boolean=true;
  users: { eid: number; Firstname: string; email: string; contact: string; }[];
  upadatedId: any;
  updfirstname: string;
  seleteddeleteId: any;


  constructor(private formBuilder: FormBuilder,
    public router: Router,) { }

  ngOnInit() {
    // Validate add details form
    this.AddStaffForm = this.formBuilder.group({
      eid: ['', Validators.required],
      Firstname: ['', Validators.required],
      contact: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],

  });
  //Validate Edit form
  this.editregisterForm = this.formBuilder.group({
    editFirstname: [''],
    editcontact: [''],
    editemail: [''],

});
// function to get details of staff
    this.getStaffList()
  }
// on Form Control function for show error
  get f() { return this.AddStaffForm.controls; }

  // On submit add new staff
  onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.AddStaffForm.invalid) {
          return;
      }
      var obj = this.AddStaffForm.value;
      this.AddStaffForm.reset();
       this.users.push(obj);
       this.AddStaffDiv=false;
       this.ListStaffDiv=true;

  }

//Function to get details of data on edit
  editDetails(id)
  {
    this.upadatedId=id;
    this.editregisterForm.patchValue({  
      editFirstname: this.users[this.upadatedId].Firstname,  
      editcontact: this.users[this.upadatedId].contact,  
      editemail: this.users[this.upadatedId].email,    
    
  }); 
  this.updfirstname=this.users[this.upadatedId].Firstname
    
  }

  // Update funtion to update data
  onUpdate()
  {
    
    this.users[this.upadatedId].Firstname =this.editregisterForm.value.editFirstname
    this.users[this.upadatedId].email =this.editregisterForm.value.editemail
    this.users[this.upadatedId].contact =this.editregisterForm.value.editcontact
      document.getElementById("closeModal").click();

 
  }
// get delete id
deleteId(id)
{
  this.seleteddeleteId=id;
}
  // OnDelete funtion to delete data 
  ondelete()
  {
   this.users.splice(this.seleteddeleteId,1);
   document.getElementById("deleteModal").click();
  }

  // ListStaff Div
  listStaffdiv()
  {
    this.AddStaffDiv=false;
    this.ListStaffDiv=true;
  }
  addStaffdiv()
  {
    this.AddStaffDiv=true;
    this.ListStaffDiv=false;
  }

  // Onload funtion to load data.
  getStaffList()
  {
    this.users=[
      { eid: 1, Firstname: 'Ram', email: 'ram@gmail.com', contact: '0000000000'  },
      { eid: 2, Firstname: 'Shyam', email: 'sh@gmail.com', contact: '1111111111'  },
      { eid: 3, Firstname: 'Mohan', email: 'moh@live.in', contact: '2222222222'  },
      { eid: 4, Firstname: 'Rohan', email: 'rohan@gmail.com', contact: '6666666666' },
      { eid: 5, Firstname: 'Sumit', email: 'sumit@live.in', contact: '9909999999'  }

    ];
    
  }

}
