import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListStaffComponent } from './components/list-staff/list-staff.component';
const routes: Routes = [

  { path: '', redirectTo: 'listStaff', pathMatch: 'full' },
 
  { path: 'listStaff', component: ListStaffComponent},
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
